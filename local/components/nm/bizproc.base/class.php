<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc as Loc;

class BizProcTestBase extends CBitrixComponent
{
    /**
     * кешируемые ключи arResult
     * @var array()
     */
    protected $cacheKeys = array();

    /**
     * дополнительные параметры, от которых должен зависеть кеш
     * @var array
     */
    protected $cacheAddon = array();

    protected $sChachePath = '';

    /**
     * подготавливает входные параметры
     * @param array $arParams
     * @return array
     */
    public function onPrepareComponentParams($aParams)
    {
        $aResult = array(
            'IBLOCK_ID' => (int)$aParams['IBLOCK_ID'],
        );

        $this->sChachePath = $this->sChachePath . serialize($aResult);
        $this->cacheKeys   = $aResult;

        return $aResult;
    }

    /**
     * проверяет подключение необходиимых модулей
     * @throws LoaderException
     */
    protected function checkModules(){

        if (!Main\Loader::includeModule('iblock') || !Main\Loader::includeModule('bizproc')){
            throw new Main\LoaderException(Loc::getMessage('MODULES_NOT_INSTALLED'));
        }
    }

    /**
     * проверяет заполнение обязательных параметров
     * @throws SystemException
     */
    protected function checkParams(){
    }

    /**
     * получение результатов
     */
    protected function getResult(){

    }

    /**
     * кеширует ключи массива arResult
     */
    protected function putDataToCache(){

        if (is_array($this->cacheKeys) && sizeof($this->cacheKeys) > 0){

            $this->SetResultCacheKeys($this->cacheKeys);
        }
    }

    /**
     * определяет читать данные из кеша или нет
     * @return bool
     */
    protected function readDataFromCache(){

        return !($this->StartResultCache(false, $this->cacheAddon, $this->sChachePath));
    }

    /**
     * Epilog
     */
    protected function executeEpilog(){

        return true;
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent(){
        try{

            $this -> checkModules();
            $this -> checkParams();

            if (!$this -> readDataFromCache()){

                $this -> getResult();
                $this -> putDataToCache();
                $this -> includeComponentTemplate();
            }

            $this -> executeEpilog();

        }catch(Exception $e){

            $this -> AbortResultCache();
            ShowError($e -> getMessage());
        }
    }
}
?>