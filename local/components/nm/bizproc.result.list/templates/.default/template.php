<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
$sNewResultLink	= '/local/test/?add_result=1&sessid='.bitrix_sessid();
?>
<table cellspacing="0" class="bx-interface-grid">
	<tbody>
	<tr class="bx-grid-head">
		<td width="1%"></td>
		<td><?=GetMessage('TITLE_FIO')?></td>
		<td><?=GetMessage('TITLE_NAME')?></td>
		<td><?=GetMessage('TITLE_COLOR')?></td>
		<td><?=GetMessage('TITLE_DATE_START')?></td>
		<td><?=GetMessage('TITLE_DATE_END')?></td>
	</tr>
	<?if(count($arResult['RECORDS'])):?>
		<?foreach ($arResult['RECORDS'] as $aResult):?>
			<?$sResultUrl	= '/services/bp/'.$arResult['BP_ID'].'/view-'.$aResult['ID'].'.php'?>
			<tr>
				<td>&nbsp;</td>
				<td>
					<a href="<?=$sResultUrl?>"><?=$aResult['CREATED_BY_PRINTABLE']?></a>
				</td>
				<td><?=$aResult['NAME']?></td>
				<td><?=$aResult['PROPERTY_choosencolor_PRINTABLE']?></td>
				<td><?=$aResult['PROPERTY_date_start_PRINTABLE']?></td>
				<td><?=$aResult['PROPERTY_date_end_PRINTABLE']?></td>
			</tr>
		<?endforeach;?>
	<?else:?>
		<tr>
			<td colspan="6"><span><?=GetMessage('RESULTS_NOT_FOUND')?></span></td>
		</tr>
	<?endif;?>
	<tr class="bx-grid-footer">
		<td colspan="7">
			<table cellpadding="0" cellspacing="0" border="0" class="bx-grid-footer">
				<tbody>
				<tr>
					<td><?=GetMessage('RESULTS_COUNT')?>: <span><?=count($arResult['RECORDS'])?></span></td>
				</tr>
				</tbody></table>
		</td>
	</tr>
	</tbody>
</table>