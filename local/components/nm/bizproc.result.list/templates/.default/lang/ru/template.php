<?
$MESS ['TITLE_FIO']         = "Кем создан (имя)";
$MESS ['TITLE_NAME']        = "Название элемента";
$MESS ['TITLE_COLOR']       = "Выбранный цвет";
$MESS ['TITLE_DATE_START']  = "Дата начала БП";
$MESS ['TITLE_DATE_END']    = "Дата окончания БП";
$MESS ['RESULTS_NOT_FOUND'] = "Заявки не найдены..";
$MESS ['RESULTS_COUNT']     = "Всего";
$MESS ['ADD_RESULTS']     = "Добавить результат";
?>