<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc as Loc;

CBitrixComponent::includeComponentClass("nm:bizproc.base");

class BizProcTestElemList extends BizProcTestBase
{

    /**
     * Поля для выборки
     * @var array
     */
    protected $aSelectedFields  = array (
        'ID',
        'NAME',
        'MODIFIED_BY_PRINTABLE',
        'CREATED_BY_PRINTABLE',
        'IBLOCK_ID',
        'PROPERTY_choosencolor',
        'PROPERTY_date_start',
        'PROPERTY_date_end',
    );

    protected $sChachePath = 'bizproc.result.list';

    /**
     * подготавливает входные параметры
     * @param array $arParams
     * @return array
     */
    public function onPrepareComponentParams($aParams)
    {
        $aResult = array(
            'IBLOCK_ID' => (int)$aParams['IBLOCK_ID'],
        );

        $this->sChachePath = $this->sChachePath . serialize($aResult);
        $this->cacheKeys   = $aResult;

        return $aResult;
    }

    /**
     * проверяет подключение необходиимых модулей
     * @throws LoaderException
     */
    protected function checkModules(){

        if (!Main\Loader::includeModule('iblock') || !Main\Loader::includeModule('bizproc')){
            throw new Main\LoaderException(Loc::getMessage('MODULES_NOT_INSTALLED'));
        }
    }

    /**
     * проверяет заполнение обязательных параметров
     * @throws SystemException
     */
    protected function checkParams(){

        if ($this->arParams['IBLOCK_ID'] <= 0) {
            throw new Main\ArgumentNullException('IBLOCK_ID');
        }
    }

    /**
     * получение результатов
     */
    protected function getResult(){

        global $CACHE_MANAGER;

        $CACHE_MANAGER->StartTagCache($this->sChachePath);
        $CACHE_MANAGER->RegisterTag("bizproc.result.list" . $this->arParams['IBLOCK_ID']);

        $this->arResult["GRID_ID"] = "bizproc_CBPVirtualDocument_".$this->arParams['IBLOCK_ID'];
        $gridOptions 		 = new CGridOptions($this->arResult["GRID_ID"]);

        $aFilter	= array(
            'IBLOCK_ID'	=> $this->arParams['IBLOCK_ID'],
            'CHECK_BP_VIRTUAL_PERMISSIONS'	=> 'read',
            '!PROPERTY_date_end'	=> false
        );

        /**
         * Нужные нам поля
         */
        $aSelectedFields	= $this->aSelectedFields;

        /**
         * Выборка результатов БП
         */
        list($oResults, $oResults2)	= CBPVirtualDocument::GetList(
            array('ID'	=> 'desc'),
            $aFilter,
            false,
            $gridOptions->GetNavParams(),
            $aSelectedFields
        );

        $aResults	= array();

        while($aResult = $oResults->Fetch()){

            $aResults[$aResult['ID']]	= $aResult;
        }

        $this->arResult["RECORDS"]	= $aResults;
        $this->arResult["BP_ID"]		= $this->arParams['IBLOCK_ID'];

        $CACHE_MANAGER->EndTagCache();
    }
}