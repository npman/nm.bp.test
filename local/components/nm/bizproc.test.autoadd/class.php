<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc as Loc;

CBitrixComponent::includeComponentClass("nm:bizproc.base");

class BizProcTestBpAutoAdd extends BizProcTestBase
{

    protected $sChachePath  = 'bizproc.autoadd';
    protected $sFilePath;

    /**
     * подготавливает входные параметры
     * @param array $arParams
     * @return array
     */
    public function onPrepareComponentParams($aParams)
    {
        $aResult = array(
            'IBLOCK_CODE' => trim($aParams['IBLOCK_CODE']),
            'IBLOCK_TYPE' => trim($aParams['IBLOCK_TYPE']),
        );

        $this->sFilePath   = __DIR__ . '/files/bp.bpt';
        $this->sChachePath = $this->sChachePath . serialize($aResult);
        $this->cacheKeys   = $aResult;

        return $aResult;
    }

    /**
     * проверяет подключение необходиимых модулей
     * @throws LoaderException
     */
    protected function checkModules(){

        if (!Main\Loader::includeModule('iblock') || !Main\Loader::includeModule('bizproc')){
            throw new Main\LoaderException(Loc::getMessage('MODULES_NOT_INSTALLED'));
        }
    }

    /**
     * проверяет заполнение обязательных параметров
     * @throws SystemException
     */
    protected function checkParams(){

        return true;
    }

    /**
     * Epilog
     */
    protected function executeEpilog(){

        $GLOBALS['APPLICATION']->SetTitle($this->arResult['IBLOCK']['NAME']);

        return true;
    }

    /**
     * получение результатов
     */
    protected function getResult(){

        /**
         * Ищем ИБ БП по поду ИБ
         */
        $aIblock	= CIBlock::GetList(array(),array('CODE'=>$this->arParams['IBLOCK_CODE']))->Fetch();

        if($aIblock) {

            global $CACHE_MANAGER;

            $CACHE_MANAGER->StartTagCache($this->sChachePath);
            $CACHE_MANAGER->RegisterTag("bizproc.autoadd" . $aIblock['ID']);

            $CACHE_MANAGER->EndTagCache();

            $this->arResult['IBLOCK'] = $aIblock;

            return true;
        }

        return false;
    }

    /**
     * Создаем БП, если не был найден
     */
    protected function createBp(){

        $sFilePath  = $this->sFilePath;
        $sIblockType= $this->arParams['IBLOCK_TYPE'];
        $sIblockCode= $this->arParams['IBLOCK_CODE'];

        $aFields = array(
            'IBLOCK_TYPE_ID' => $sIblockType,
            'LID' => 's1',
            'CODE' => $sIblockCode,
            'NAME' => '[test] Выбор цвета',
            'ACTIVE' => 'Y',
            'SORT' => 1,
            'PICTURE' => false,
            'DESCRIPTION' => $v1,
            'DESCRIPTION_TYPE' => 'text',
            'WORKFLOW' => 'N',
            'BIZPROC' => 'Y',
            'VERSION' => 1,
            'ELEMENT_ADD' => 'Выбрать цвет',
            'GROUP_ID' =>
                array(
                    2 => 'R',
                ),
        );

        $oIblock = new CIBlock();

        $iIblockId = $oIblock->Add($aFields);

        /**
         * Если успешно создали ИБ, то создаем шаблон БП
         */
        if($iIblockId){

            $aFields['ID']	= $iIblockId;

            $aBizProcFields	= array (
                'DOCUMENT_TYPE' =>   array (
                    0 => 'bizproc',
                    1 => 'CBPVirtualDocument',
                    2 => 'type_' . $iIblockId,
                ),
                'AUTO_EXECUTE' => '1',
                'NAME' => 'Шаблон бизнес-процесса',
                'DESCRIPTION' => '',
                'TEMPLATE' =>   array (
                    0 =>     array (
                        'Type' => 'SequentialWorkflowActivity',
                        'Name' => 'Template',
                    ),
                ),
                'PARAMETERS' => NULL,
                'VARIABLES' =>   array (  ),
                'USER_ID' => 1,
                'MODIFIER_USER' =>   new CBPWorkflowTemplateUser(CBPWorkflowTemplateUser::CurrentUser),
            );

            $iTemplateId	= CBPWorkflowTemplateLoader::Add($aBizProcFields);

            /**
             * Если шаблон был успешно создан, то импортируем шаблон из файла
             */
            if($iTemplateId){

                try
                {
                    $f = fopen($sFilePath, "rb");
                    $datum = fread($f, filesize($sFilePath));
                    fclose($f);

                    $r = CBPWorkflowTemplateLoader::ImportTemplate(
                        $iTemplateId,
                        array('bizproc', 'CBPVirtualDocument', 'type_' . $iIblockId),
                        1,
                        'Шаблон бизнес-процесса',
                        '',
                        $datum
                    );

                    return $aFields;
                }
                catch (Exception $e)
                {
                    /**
                     * Что-то пошло не так..
                     */
                    ShowError(GetMessage('ERROR'));
                }
            }
        }
    }

    /**
     * выполняет логику работы компонента
     */
    public function executeComponent(){
        try{

            $this -> checkModules();
            $this -> checkParams();

            if (!$this -> readDataFromCache()){

                /**
                 * Если не находим ИБ, значит нужно создать БП
                 */
                if(!$this -> getResult()){

                    $this->createBp();
                    $this->getResult();
                }

                $this -> putDataToCache();
            }

            $this -> executeEpilog();

            return $this->arResult['IBLOCK'];

        }catch(Exception $e){

            $this -> AbortResultCache();
            ShowError($e -> getMessage());
        }
    }
}