<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use \Bitrix\Main;
use \Bitrix\Main\Localization\Loc as Loc;

Loc::loadMessages(__FILE__); 

$arComponentParameters = array(
    'GROUPS' => array(
    ),
    'PARAMETERS' => array(
        'IBLOCK_TYPE' => array(
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('BP_IBLOCK_TYPE'),
            'TYPE' => 'TEXT',
        ),
        'IBLOCK_CODE' => array(
            'PARENT' => 'BASE',
            'NAME' => Loc::getMessage('BP_IBLOCK_CODE'),
            'TYPE' => 'TEXT',
        ),
    )
);